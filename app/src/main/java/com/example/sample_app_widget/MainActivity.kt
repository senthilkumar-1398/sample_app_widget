package com.example.sample_app_widget

import android.content.*
import android.media.MediaPlayer
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.widget.LinearLayout


class MainActivity : BaseActivity() {
    private var mService: LocationUpdateService? = null
    private var mBroadcastReceiver: BroadcastReceiver? = null
    private var mMediaPlayer: MediaPlayer? = null


    private val mServiceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder: LocationUpdateService.LocalBinder =
                service as LocationUpdateService.LocalBinder
            mService = binder.service
            Log.e("TAG", "onServiceConnected: ")
            mService?.requestLocationUpdates()
        }

        override fun onServiceDisconnected(name: ComponentName) {
            mService = null
            Log.e("TAG", "onServiceDisconnected: ")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(mBroadcastReceiver)
    }

    override fun onResume() {
        super.onResume()
        mBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent) {
                Log.d("TAG", "onReceive: Home")
                /* Toast.makeText(context, "Message is: "+ intent.getStringExtra("message"), Toast.LENGTH_LONG)
                    .show();*/
                val action = intent.action
                when (action) {
                    "broadCastName" -> {
                        val mess = intent.getStringExtra("message")
                        val linearLayout = findViewById<LinearLayout>(R.id.color)
                        linearLayout.setBackgroundResource(R.color.teal_200)
                    }
                }
            }
        }
        val filter = IntentFilter("broadCastName")
        registerReceiver(mBroadcastReceiver, filter)
    }
}