package com.example.sample_app_widget;

/**
 * Created by Nivetha S on 28-01-2021.
 */
public class GPSChangeNotifyEvent {
    private boolean isGPSEnabled;

    public GPSChangeNotifyEvent(boolean isGPSEnabled) {
        this.isGPSEnabled = isGPSEnabled;
    }

    public boolean isGPSEnabled() {
        return isGPSEnabled;
    }

    public void setGPSEnabled(boolean isGPSEnabled) {
        this.isGPSEnabled = isGPSEnabled;
    }
}
