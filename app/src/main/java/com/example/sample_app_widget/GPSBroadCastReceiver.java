package com.example.sample_app_widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by Nivetha S on 28-01-2021.
 */
public class GPSBroadCastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        LocationManager mLocationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);

        if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Log.e("GPS ", "onReceive: true");
            EventBus.getDefault().post(new GPSChangeNotifyEvent(true));
        } else {
            Log.e("GPS ", "onReceive: false");
            EventBus.getDefault().post(new GPSChangeNotifyEvent(false));
        }
    }
}
