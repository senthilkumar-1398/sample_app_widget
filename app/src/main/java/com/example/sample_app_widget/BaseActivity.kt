package com.example.sample_app_widget

import android.content.*
import android.content.IntentSender.SendIntentException
import android.location.LocationManager
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

open class BaseActivity : AppCompatActivity() {

    private val TAG = "BaseActivity"
    private var mService: LocationUpdateService? = null
    private var mBound = false
    private var mGpsBroadCastReceiver: GPSBroadCastReceiver? = null
    private var mGpsAlertDialog: AlertDialog? = null



    private fun enablePermission(): Boolean {
        val locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
        Log.e(TAG, "enablePermission: ")
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Log.e(TAG, "enablePermission: Enabled")
            bindService(
                Intent(this, LocationUpdateService::class.java), mServiceConnection,
                BIND_AUTO_CREATE
            )
            return true
        } else {
            Log.e(TAG, "enablePermission: Disabled")
            EventBus.getDefault().postSticky(GPSChangeNotifyEvent(false))
            return false
        }
    }

    private val mServiceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as LocationUpdateService.LocalBinder
            mService = binder.service
            mBound = true
            Log.e(TAG, "onServiceConnected: ")
            mService?.requestLocationUpdates()
        }

        override fun onServiceDisconnected(name: ComponentName) {
            mService = null
            mBound = false
            Log.e(TAG, "onServiceDisconnected: ")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EventBus.getDefault().register(this)
        //Check Gps Change
        mGpsBroadCastReceiver = GPSBroadCastReceiver()
        registerReceiver(
            mGpsBroadCastReceiver,
            IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION)
        )
    }


    override fun onStart() {
        super.onStart()
        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        enablePermission()
    }

    override fun onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            Log.e(TAG, "onStop:mBound ")
            unbindService(mServiceConnection)
            mBound = false
        }
        super.onStop()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(gpsChangeNotifyEvent: GPSChangeNotifyEvent) {
        Log.e(TAG, "onMessageEvent: ShoW Dialog")
        if (!gpsChangeNotifyEvent.isGPSEnabled) {
            stopService(Intent(this, LocationUpdateService::class.java))
            gpsEnableRequest()
        } else {
            if (mGpsAlertDialog!!.isShowing && mGpsAlertDialog != null) {
                mGpsAlertDialog!!.dismiss()
            }
            bindService(
                Intent(this, LocationUpdateService::class.java), mServiceConnection,
                BIND_AUTO_CREATE
            )
        }
    }



    private fun gpsEnableRequest() {
        val mLocationRequest = LocationRequest.create()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = (30 * 1000).toLong()
        mLocationRequest.fastestInterval = (5 * 1000).toLong()
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest)
        val task = LocationServices.getSettingsClient(this).checkLocationSettings(builder.build())
        task.addOnCompleteListener { task ->
            try {
                val response = task.getResult(
                    ApiException::class.java
                )
                Log.e(
                    TAG,
                    "onComplete:isGpsUsable " + response!!.locationSettingsStates.isGpsUsable
                )
                bindService(
                    Intent(this@BaseActivity, LocationUpdateService::class.java),
                    mServiceConnection,
                    BIND_AUTO_CREATE
                )
            } catch (exception: ApiException) {
                when (exception.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                        val resolvable = exception as ResolvableApiException
                        try {
                            Log.e(TAG, "onComplete:GPS Open requested ")
                            resolvable.startResolutionForResult(this@BaseActivity, 1000)
                        } catch (e: SendIntentException) {
                            e.printStackTrace()
                        }
                    }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE ->
                        showGPSDisabledAlertToUser()
                }
            }
        }
    }

    private fun showGPSDisabledAlertToUser() {
        val mGpsAlertDialogBuilder = AlertDialog.Builder(this)
        mGpsAlertDialogBuilder.setMessage(
            "GPS is disabled in your device " + "\t" +
                    "Need GPS to access the App" + "\t" +
                    "Would you like to enable it?"
        )
            .setCancelable(false)
            .setTitle("Enable GPS")
            .setPositiveButton(
                "OK"
            ) { dialog, id ->
                gpsEnableRequest()
                dialog.cancel()
            }
        mGpsAlertDialog = mGpsAlertDialogBuilder.create()
        mGpsAlertDialog!!.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy:")
//        val serviceIntent = Intent(this, LocationUpdateService::class.java)
//        serviceIntent.putExtra("inputExtra", "Foreground Service Example in Android")
//        ContextCompat.startForegroundService(this, serviceIntent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            1000 -> when (resultCode) {
                RESULT_OK -> {
                    Log.e(TAG, "onActivityResult:ResultOk ")
                    bindService(
                        Intent(this@BaseActivity, LocationUpdateService::class.java),
                        mServiceConnection,
                        BIND_AUTO_CREATE
                    )
                }
                RESULT_CANCELED -> {
                    gpsEnableRequest()
                }
            }
        }
    }
}