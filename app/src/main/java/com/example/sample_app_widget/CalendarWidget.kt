package com.example.sample_app_widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.util.Log
import android.widget.RemoteViews
import com.google.android.gms.maps.model.LatLng
import java.util.*


/**
 * Implementation of App Widget functionality.
 */
class CalendarWidget : AppWidgetProvider() {
    private var mRemoteViews: RemoteViews? = null
    private var mLatLng: LatLng? = null
    val TAG="CalendarWidget"

    override fun onDisabled(context: Context?) {
        super.onDisabled(context)
        Log.d(TAG, "onDisabled: ")
    }

    override fun onEnabled(context: Context?) {
        super.onEnabled(context)
        Log.d(TAG, "onEnabled: ")
    }

    override fun onDeleted(context: Context?, appWidgetIds: IntArray?) {
        super.onDeleted(context, appWidgetIds)
        Log.d(TAG, "onDeleted: ")
    }

    override fun onUpdate(
        context: Context?,
        appWidgetManager: AppWidgetManager?,
        appWidgetIds: IntArray?
    ) {
        super.onUpdate(context, appWidgetManager, appWidgetIds)
        Log.d(TAG, "onUpdate: ")

    }

    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)
        when (intent!!.action) {
            "latlng" -> {
                mRemoteViews = RemoteViews(context?.packageName, R.layout.calendar_widget)

                mLatLng = intent.extras?.getParcelable("lat")

                Log.d("TAG", "onReceive: 1 ${mLatLng.toString()}")
                val mMapIntent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?q=loc:${mLatLng?.latitude},${mLatLng?.longitude}(MyLocation)")
                )
                context?.startActivity(mMapIntent)

                val mPendingIntent = PendingIntent.getActivity(context, 0, mMapIntent, 0)
                var address =
                    getCompleteAddressString(mLatLng!!.latitude, mLatLng!!.longitude, context)
                Log.d("TAG", "onReceive: $address")
                mRemoteViews?.setTextViewText(R.id.address, address)
                mRemoteViews?.setOnClickPendingIntent(R.id.section_layout, mPendingIntent)
                AppWidgetManager.getInstance(context).updateAppWidget(
                    ComponentName(context!!, CalendarWidget::class.java), mRemoteViews
                )
            }
        }
    }
}

private fun getCompleteAddressString(
    LATITUDE: Double,
    LONGITUDE: Double,
    context: Context?
): String? {
    var mCurrentAddress = ""
    val mGeocoder = Geocoder(context, Locale.getDefault())
    try {
        val mAddressList: List<Address>? = mGeocoder.getFromLocation(LATITUDE, LONGITUDE, 1)
        if (mAddressList != null) {
            val returnedAddress: Address = mAddressList[0]
            val strReturnedAddress = StringBuilder("")
            for (i in 0..returnedAddress.maxAddressLineIndex) {
                strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n")
            }
            mCurrentAddress = strReturnedAddress.toString()
            Log.w("My Current loction address", strReturnedAddress.toString())
        } else {
            Log.w("My Current loction address", "No Address returned!")
        }
    } catch (e: Exception) {
        e.printStackTrace()
        Log.w("My Current loction address", "Canont get Address!")
    }
    return mCurrentAddress
}

